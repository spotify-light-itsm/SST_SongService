

<h1 align="center"> itsm-songservice </h1>
<p align="center">
  Microservice Description
</p>

---

## Table of Contents:
- [Introduction](#introduction)
- [API Documentation](#api-documentation)
- [Features](#features)

## Introduction
This microservice was built by *Bijelonjic Stjepan* and *Fingerlos David* in 2020 for a project at the University of Applied Science in Salzburg.

It is designed as part a "Spotify Light" song streaming appplication.
It is build as a Spring Boot Java Application 

This microservice is built for beeing run in a Docker container. Therefore a Docker-Image for this service is provided.

## API Documentation
All features are provided by REST calls. A detailed REST API endpoint documentation can be found here:
[Song Service REST API Documentation](https://app.swaggerhub.com/apis-docs/sblnc0/songservice/1.1.0#/)

## Features
This microservice includes following features:
- uploading metadata of a song and store them to a database
- playlists features:
  - create a playlist
  - modify a playlist 
  - delete a playlist
- search for songs by:
  - id
  - name
  - title
  - genre

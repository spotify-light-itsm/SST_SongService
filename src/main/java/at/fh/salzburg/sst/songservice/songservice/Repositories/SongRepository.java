package at.fh.salzburg.sst.songservice.songservice.Repositories;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import at.fh.salzburg.sst.songservice.songservice.Models.Song;

@Repository
@Transactional
public interface SongRepository extends CrudRepository<Song, Integer> {

	@Transactional(timeout = 10)
	ArrayList<Song> getSongsByGenre(String genre);

	@Transactional(timeout = 10)
	Song findBySongId(int songId);
	
	@Query(value = "select TOP 5 song_id from song where upper(title) LIKE :searchterm OR upper(artist) LIKE :searchterm", nativeQuery = true) 
	List<Integer> searchfunction(@Param("searchterm") String searchterm);
								 
}


package at.fh.salzburg.sst.songservice.songservice.Models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

@Entity
//@Table(name = "SONGS")
public class Song {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int songId;
    private String title = "";
    private String artist = "";
    private int duration = -1;
    private String filepath = "";
    private String imagepath = "";
    private String creationDate = "";
    private Genre genre;
    private long sizeInKB = 0;
    private Boolean deletedFlag = false;

    public enum Genre {
        RAP, HIPHOP, POP, TECHNO, ROCK, OLDIES, EDM, HOUSE, COUNTRY
    }

    // Default Constructor
    protected Song() {}
    
    // Demo Constructor
    public Song(String title, String artist) {
        this.title = title;
        this.setArtist(artist);
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Functions
    public String toString(){
        return "Artist: " + this.artist + 
                    "<br>Songtitle: " + this.title +
                    "<br>Duration: " + this.duration + 
                    "<br>Genre: " + this.genre;
    }

    public String toJSON(){
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String ret = "";
        
        try {
            ret = ow.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            ret = e.getMessage();
        }

        return ret;
    }

    public void deleteSong(){
        /* this function deletes the path to the song and sets
         * the deleted flag. It does NOT delete the song physically 
         * from the song database. The Song Metadata also stay the same.*/
        this.filepath = "";
        this.setDeletedFlag();
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Getters and Setters 
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    public Boolean getDeletedFlag(){return deletedFlag;}
    public void setDeletedFlag(){deletedFlag = true;}

    public int getSongId(){ return songId;}
    public String getArtist() { return artist;}

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public int getDuration() {return duration;}

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getFilepath() {return filepath;}

    public void setFilepath(String filepath) {
        this.filepath = filepath;
    }

    public String getImagepath() {return imagepath;}

    public void setImagepath(String imagepath) {
        this.imagepath = imagepath;
    }

    public String getCreationDate() {return creationDate;}

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public Genre getGenre() {return genre;}

    public void setGenre(Genre genre) {
        this.genre = genre;
    }



    public long getSizeInKB() {return sizeInKB;}

    public void setSizeInKB(long sizeInKB) {
        this.sizeInKB = sizeInKB;
    }


    public String getTitle(){return this.title;}

    public void setTitle(String title){
        this.title=title;
    }



}

package at.fh.salzburg.sst.songservice.songservice.Controller;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import at.fh.salzburg.sst.songservice.songservice.Models.Album;
import at.fh.salzburg.sst.songservice.songservice.Models.Playlist;
import at.fh.salzburg.sst.songservice.songservice.Models.Responses;
import at.fh.salzburg.sst.songservice.songservice.Models.Song;
import at.fh.salzburg.sst.songservice.songservice.Models.Song.Genre;
import at.fh.salzburg.sst.songservice.songservice.Repositories.AlbumRepository;
import at.fh.salzburg.sst.songservice.songservice.Repositories.PlaylistRepository;
import at.fh.salzburg.sst.songservice.songservice.Repositories.SongRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

@RestController
public class MyRestController {

    // Connect the repository
    @Autowired
    SongRepository songRepo;

    @Autowired
    PlaylistRepository playlistRepo;

    @Autowired
    AlbumRepository albumRepo;

    Responses responses = new Responses();

    public String getFirstSong() {
        List<Song> songs = new ArrayList<>();
        songRepo.findAll().forEach(song -> songs.add(song));

        return songs.get(0).toString();
    }

    // Demo function for testing availability of service
    @RequestMapping("/initSongs")
    public String initSongs() {
        storeDemoSong();
        return getFirstSong();
    }

    @GetMapping("/song/getById/{songId}")
    public String getSongById(@PathVariable final int songId) {
        /*
         * this function takes a songId and returns the song details as strings
         */

        return songRepo.findBySongId(songId).toJSON();
    }

    @GetMapping("/song/getByTitle/{songTitle}")
    public String getSongsByTitle(@PathVariable final String songTitle){
        List<Song> songs = getSongsByTitleFromDatabase(songTitle);

        return getSongsAsJSON(songs);
    }

    @GetMapping("/song/getByArtist/{songArtist}")
    public String getSongsByArtist(@PathVariable final String songArtist){
        List<Song> songs = getSongsByArtistFromDatabase(songArtist);

        return getSongsAsJSON(songs);
    }

    @GetMapping("/song/getByGenre/{songGenre}")
    public String getSongsByGenre(@PathVariable final Genre songGenre){
        List<Song> songs = getSongsByGenreFromDatabase(songGenre);

        return getSongsAsJSON(songs);
    }

    @PostMapping("/song/uploadMeta")
    public String uploadSongMetadata(@RequestBody Song songWithMeta) {
        /*
         * this function takes songData as JSON and stores the song to the songRepo
         */
        String ret = "";
        Boolean search = true;
        // check if song already exists
        // get Songs with this title
        List<Song> songsWithSameTitle = getSongsByTitleFromDatabase(songWithMeta.getTitle());

        for (Song song : songsWithSameTitle){
            if (song.getArtist().equals(songWithMeta.getArtist()) && (!song.getDeletedFlag())){
                ret = "Song already exists";
                search = false;
            }
        }

        if (Boolean.TRUE.equals(search)){
            // store the new song to database
            songRepo.save(songWithMeta);
            ret = songWithMeta.toJSON();
        }

        return ret;

    }

    // delete Song
    @DeleteMapping("/song/delete/{id}")
    public String deleteSong(@PathVariable final int id){
        songRepo.findBySongId(id).deleteSong();
        songRepo.save(songRepo.findBySongId(id));
        return "song with id: " + id + " deleted";
    }

    @GetMapping("song/getAllInDatabase")
    public String getAllSongsInDatabase(){
        /* this function returns all Songs stored
           in database songRepo */
        String ret = "";
        List<Song> songs = new ArrayList<>();
        songRepo.findAll().forEach(song->songs.add(song));

        ret = getSongsAsJSON(songs);     

        return ret;
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~ PLAYLIST FUNCTIONS ~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    // create Playlist
    @PostMapping("/playlist/create")
    public String createPlaylist(@RequestBody Playlist newPlaylist) {
        playlistRepo.save(newPlaylist);
        
        String ret ="";
        ret = newPlaylist.toJSON();

        return ret;
    }

    // Add song to playlist
    @PutMapping("/playlist/addSong/{playlistId}/{songId}")
    public String addSongToPlaylist(@PathVariable final int songId, @PathVariable final int playlistId) {
        Optional<Playlist> optplaylist = playlistRepo.findById(playlistId);
        if (optplaylist.isPresent()){
            Playlist playlist = optplaylist.get();    
            if (playlist != null){
                playlist.addSongToPlaylist(songId);
                return playlist.toJSON();
            }
        } 
        return responses.playlistNotFound();
        
    }

    // Get all songs in a playlist
    @GetMapping("/playlist/getSongs/{playlistId}")
    public String getAllSongsInPlaylist(@PathVariable final int playlistId){
        /* this function returns all Songs stored
           in database songRepo */

        String ret = responses.playlistNotFound();

        ArrayList<Playlist> playlists = new ArrayList<>();
        playlistRepo.findAll().forEach(playlist->playlists.add(playlist));

        Playlist playlist;
        // Workaround to find playlist by id
        for (Playlist tempPlaylist : playlists) {
            if (tempPlaylist.getPlaylistId() == playlistId){
                playlist = tempPlaylist;
                
                ArrayList<Song> songs = new ArrayList<>();
                for (int songId : playlist.getAllSongsInPlaylist()) {
                    songs.add(songRepo.findBySongId(songId));
                }

                playlist.getAllSongsInPlaylist();
                if (!songs.isEmpty()){
                    ret = getSongsAsJSON(songs); 
                } else {
                    ret = responses.playlistIsEmpty();
                } 
            }
        }
        return ret;
    }

    // Get all Playlists from one owner:
    @GetMapping("/playlist/getByOwner/{ownerId}")
    public String getAllPlaylistsByOwnerId(@PathVariable final int ownerId){
        /* this function returns all Songs stored
           in database songRepo */

        String ret = responses.playlistNotFound();

        ArrayList<Playlist> playlists = new ArrayList<>();
        playlistRepo.findAll().forEach(playlist->playlists.add(playlist));

        ArrayList<Playlist> ownerPlaylists = new ArrayList<>();
        // Workaround to find playlist by id
        for (Playlist tempPlaylist : playlists) {
            if (tempPlaylist.getOwnerId() == ownerId){
                if (ret.equals(responses.playlistNotFound())){ ret = "[";}
                ownerPlaylists.add(tempPlaylist);
                
                ret += tempPlaylist.toJSON();
                ret += ", ";    
            }
        }

        if (!ret.equals(responses.playlistNotFound())){
            ret = ret.substring(0, ret.length()-2);
            ret += "]";
        }
        
        return ret;
    }

    // Delete song from playlist
    @DeleteMapping("/playlist/deleteSong/{playlistId}/{songId}")
    public String deleteSongFromPlaylist(@PathVariable final int playlistId, @PathVariable final int songId) {
        if (playlistRepo.findById(playlistId).isPresent()){
            // Get playlist from playlist repo
            Playlist playlist = playlistRepo.findById(playlistId).get();
            
            // delete song from playlist
            playlist.deleteSongById(songId);
            
            // update playlist in repo        
            playlistRepo.save(playlist);
            
            return playlist.toJSON();
        } else {
            return responses.playlistNotFound();
        }
    }

    // get playlist by id
    @GetMapping("/playlist/getById/{id}")
    public String getPlaylistById(@PathVariable final int id){
        if(!playlistRepo.findById(id).isEmpty()){
            return playlistRepo.findById(id).get().toJSON();
        } else {
            return responses.playlistNotFound();
        }
    }

    // DELETE Playlist
    @DeleteMapping("/playlist/delete/{id}")
    public String deletePlaylist(@PathVariable final int id){
        playlistRepo.deleteById(id);
        return responses.playlistDeleted();
    }


    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~ ALBUM FUNCTIONS ~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    // create Album
    @PostMapping("/album/create")
    public String createAlbum(@RequestBody Album newAlbum) {
        String ret = "";

        // check if album already exists
        // get all albums
        ArrayList<Album> allAlbums = new ArrayList<>();
        albumRepo.findAll().forEach(album->allAlbums.add(album));

        // get all albums for this artist
        for (Album album : allAlbums) {
            if((album.getArtist().equals(newAlbum.getArtist()))&&(album.getName().equals(newAlbum.getName()))){
                return album.toJSON();
            }
        }

        // if we get here, the album does not exist yet
        albumRepo.save(newAlbum);
        ret = newAlbum.toJSON();

        return ret;
    }

    // Add song to album
    @PostMapping("/album/addSong/{albumId}/{songId}")
    public String addSongToAlbum(@PathVariable final int songId, @PathVariable final int albumId) {
        Optional<Album> optalbum = albumRepo.findById(albumId);
        String ret = responses.albumNotFound();

        if (optalbum.isPresent()){
            if (optalbum.get() != null){
                Album album = optalbum.get();
                
                album.addSongToAlbum(songId);
                return songRepo.findBySongId(songId).toJSON();
        
            }
        }
        return ret;
        
    }

    // get album by id
    @GetMapping("/album/getById/{id}")
    public String getAlbumById(@PathVariable final int id){
        if(!albumRepo.findById(id).isEmpty() && albumRepo.findById(id).isPresent()){
            return albumRepo.findById(id).get().toJSON();
        } else {
            return responses.albumNotFound();
        }
    }

    // Get all songs by album
    @GetMapping("/album/getSongs/{id}")
    public String getAllSongsInAlbum(@PathVariable final int id){
        if(!albumRepo.findById(id).isEmpty() && albumRepo.findById(id).isPresent()){
            ArrayList<Song> allSongsInAlbum = new ArrayList<>();
            for (Integer songId : albumRepo.findById(id).get().getAllSongsInAlbum()) {
                allSongsInAlbum.add(songRepo.findBySongId(songId));
            }
            
            return getSongsAsJSON(allSongsInAlbum);
        } else {
            return responses.albumNotFound();
        }
    }

    // DELETE Album
    @DeleteMapping("/album/delete/{id}")
    public String deleteAlbum(@PathVariable final int id){
        if (albumRepo.findById(id).isPresent()){
            albumRepo.deleteById(id);
        }
        return responses.albumDeleted();
    }

    // Delete song from album
    @DeleteMapping("/album/deleteSong/{albumId}/{songId}")
    public String deleteSongFromAlbum(@PathVariable final int albumId, @PathVariable final int songId) {
        if (albumRepo.findById(albumId).isPresent()){
            // Get album from album repo
            Album album = albumRepo.findById(albumId).get();
            
            // delete song from album
            album.deleteSongById(songId);
            
            // update album in repo        
            albumRepo.save(album);
            return album.toJSON();
        } else {
            return responses.albumNotFound();
        }
    }

    
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ADDITIONAL FUNCTIONS
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    // search function
    @GetMapping("/seachFor/{searchterm}")
    public String searchFor(@PathVariable final String searchterm){
        /* the function searchFor searches via sql query for 
         * songs containing the searchterm in
         * artist name or song name and returns the top 5 songs
         * from the song database as JSON */

        ArrayList<Song> songs = new ArrayList<>();

        String searchtermLike = "%" + searchterm.toUpperCase() + "%";
        for (Integer id : songRepo.searchfunction(searchtermLike)){
            songs.add(songRepo.findBySongId(id));
        }

        return getSongsAsJSON(songs);
    } 




    @Autowired
    public void storeDemoSong(){
        Song demosong = new Song("Demo Title", "Demo Artist");
        Song demosong2 = new Song("Fick 31er", "Joker Bra");
        Song demosong3 = new Song("Mood", "24kGoldn");
        Song demosong4 = new Song("Bilnding Lights", "The Weeknd");

        songRepo.save(demosong);
        songRepo.save(demosong2);
        songRepo.save(demosong3);
        songRepo.save(demosong4);
        
    }

    private List<Song> getSongsByTitleFromDatabase(String title){
        List<Song> songs = new ArrayList<>();
        songRepo.findAll().forEach(song->songs.add(song));

        List<Song> retSongs = new ArrayList<>();

        for (Song song : songs){
            if (song.getTitle().equals(title)){
                retSongs.add(song);
            }
        }

        return retSongs;
    }

    private List<Song> getSongsByArtistFromDatabase(String artist){
        List<Song> songs = new ArrayList<>();
        songRepo.findAll().forEach(song->songs.add(song));

        List<Song> retSongs = new ArrayList<>();

        for (Song song : songs){
            if (song.getArtist().equals(artist) && (song.getDeletedFlag() == false)){
                retSongs.add(song);
            }
        }

        return retSongs;
    }

    private List<Song> getSongsByGenreFromDatabase(Genre genre){
        List<Song> songs = new ArrayList<>();
        songRepo.findAll().forEach(song->songs.add(song));

        List<Song> retSongs = new ArrayList<>();

        for (Song song : songs){
            if (song.getGenre() == genre){
                retSongs.add(song);
            }
        }

        return retSongs;
    }

    private String getSongsAsJSON(List<Song> songs){
        String ret ="";
        if (songs.size() > 1){
            ret = "[";
            for(int i=0; i < songs.size(); i+=1){
                ret += songs.get(i).toJSON();
                if (i < songs.size()-1){
                    ret += ",";
                } 
            }
            ret += "]";
        } else if (songs.size() == 1 ) {
            ret = songs.get(0).toJSON();
        }

        return ret;
    }
    
}

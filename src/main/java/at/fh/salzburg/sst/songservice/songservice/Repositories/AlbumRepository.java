package at.fh.salzburg.sst.songservice.songservice.Repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import at.fh.salzburg.sst.songservice.songservice.Models.Album;

@Repository
@Transactional
public interface AlbumRepository extends CrudRepository<Album, Integer> {

	@Transactional(timeout = 10)
	Album findByAlbumId(int albumId);
	
	// build query to search for something: 
	// https://stackoverflow.com/questions/60607545/spring-boot-jpa-or-sql-query-to-find-most-common-item-in-joined-tables
}
package at.fh.salzburg.sst.songservice.songservice.Models;

import java.util.ArrayList;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

@Entity
public class Album {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int albumId;
    private String name = "";
    private String artist = "";
    private String releaseDate ="";
    
    private String imagepath = "";
    
    private ArrayList<Integer> songs = new ArrayList<>();

    // Default Constructor
    protected Album(){}
    
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Functions
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public String toJSON(){
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String ret = "";
        
        try {
            ret = ow.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            ret = e.getMessage();
        }

        return ret;
    }

    public void addSongToAlbum(int songId){
        songs.add(songId);
    }

    public void addSongsToAlbum(ArrayList<Song> newSongs){
        for (Song song : newSongs) {
            songs.add(song.getSongId());
        } 
    }

    public ArrayList<Integer> getAllSongsInAlbum(){
        return songs;
    }

    public void deleteSongById(int id){
        for(int i=0; i<songs.size(); i+=1){
            if (songs.get(i) == id){
                songs.remove(i);
                return;
            }
        }
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Getters and Setters 
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public int getAlbumId(){ return albumId;}

    public String getImagepath() {return imagepath;}

    public void setImagepath(String imagepath) {
        this.imagepath = imagepath;
    }

    public String getReleaseDate() {return releaseDate;}

    protected void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }


    public String getName(){return this.name;}

    public void setName(String name){
        this.name=name;
    }

    public String getArtist(){return this.artist;}

    public void setArtist(String artist){
        this.artist=artist;
    }



}

package at.fh.salzburg.sst.songservice.songservice.Models;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

@Entity
public class Playlist{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int playlistId;
    private String name = "";
    private int ownerId = 0;
    private int duration = -1;
    private String imagepath = "";
    private String creationDate = "";
    
    private ArrayList<Integer> songs = new ArrayList<>();

    // Default Constructor
    protected Playlist() {
        // set creation date to current date
        this.creationDate = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
    }
    
    // Demo Constructor
    public Playlist(String name, int ownerId) {
        this.name = name;
        this.ownerId = ownerId;

        // set creation date to current date
        this.creationDate = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Functions
    public String toJSON(){
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String ret = "";
        
        try {
            ret = ow.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            ret = e.getMessage();
        }

        return ret;
    }

    public void addSongToPlaylist(int songId){
        songs.add(songId);
    }

    public void addSongsToPlaylist(ArrayList<Song> newSongs){
        for (Song song : newSongs) {
            songs.add(song.getSongId());
        } 
    }

    public ArrayList<Integer> getAllSongsInPlaylist(){
        return songs;
    }

    public void deleteSongById(int id){
        for(int i=0; i<songs.size(); i+=1){
            if (songs.get(i) == id){
                songs.remove(i);
                return;
            }
        }
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Getters and Setters 
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public int getPlaylistId(){ return playlistId;}


    public int getDuration() {return duration;}

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getImagepath() {return imagepath;}

    public void setImagepath(String imagepath) {
        this.imagepath = imagepath;
    }

    public String getCreationDate() {return creationDate;}

    protected void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }


    public String getName(){return this.name;}

    public void setName(String name){
        this.name=name;
    }

    public int getOwnerId(){return this.ownerId;}

    public void setOwnerId(int ownerId){
        this.ownerId=ownerId;
    }



}

package at.fh.salzburg.sst.songservice.songservice.Interfaces;

public interface ArtistAndTitle {
    String getArtist();
    String getTitle();
}

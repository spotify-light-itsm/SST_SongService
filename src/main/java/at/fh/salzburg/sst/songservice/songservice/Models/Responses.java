package at.fh.salzburg.sst.songservice.songservice.Models;

public class Responses {
    static final String playlistNotFound = "Playlist not found";
    static final String albumNotFound = "Album not found";
    static final String playlistIsEmpty = "Playlist is empty";
    static final String playlistDeleted = "Playlist deleted";
    static final String albumDeleted = "Album deleted";
    
    public String playlistNotFound(){
        return playlistNotFound;
    }

    public String albumNotFound(){
        return albumNotFound;
    }

    public String playlistIsEmpty(){
        return playlistIsEmpty;
    }

    public String playlistDeleted(){
        return playlistDeleted;
    }

    public String albumDeleted(){
        return albumDeleted;
    }
}

package at.fh.salzburg.sst.songservice.songservice.Repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import at.fh.salzburg.sst.songservice.songservice.Models.Playlist;

@Repository
@Transactional
public interface PlaylistRepository extends CrudRepository<Playlist, Integer> {

	@Transactional(timeout = 10)
	Playlist findByPlaylistId(int playlistId);
	
	// build query to search for something: 
	// https://stackoverflow.com/questions/60607545/spring-boot-jpa-or-sql-query-to-find-most-common-item-in-joined-tables
}
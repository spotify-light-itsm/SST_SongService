openapi: 3.0.0
info:
  title: SongService API
  version: 1.1.0
  description: This documentation and the underlaying API is created by<br>
                <i>David Fingerlos and Stjepan Bijelonjic</i><br>
               as part of an Specific Software Technologies Project<br>
               at the University of applied Science Salzburg.<br>
               last updated 08-12-2020<br>
               
servers:
  - url: 'http://localhost:8080/'
paths:
  '/song/getById/{id}':
    get:
      summary: Returns a song by its id.
      description: >-
        duration is returned in seconds<br> deletedFlag shows if the song is
        deleted. creation date is set manually.
      tags:
        - song
      parameters:
        - in: path
          name: id
          schema:
            type: integer
          required: true
          example: 4
      responses:
        '200':
          description: Song as JSON
          content:
            application/json:
              schema:
                type: object
                example:
                  - songId: 38
                    title: Not Afraid
                    artist: Eminem
                    duration: 3488
                    filepath: songs/eminem/not_afraid.mp3
                    imagepath: imgs/eminem/not_afraid.jpg
                    creationDate: 08-02-2020
                    genre: HIPHOP
                    sizeInKB: 7449
                    deletedFlag: false
  '/song/getByTitle/{title}':
    get:
      summary: Returns all songs by title
      description: >-
        This function returns all songs matching the title. <br> Therefore an
        exact match is needed. For flexible searching see <br> /searchFor/
        below.
      tags:
        - song
      parameters:
        - in: path
          name: title
          schema:
            type: string
          required: true
          example: Runaway
      responses:
        '200':
          description: Songs as JSON array
          content:
            application/json:
              schema:
                type: object
                example:
                  - songId: 38
                    title: Runaway
                    artist: Bon Jovi
                    duration: 3488
                    filepath: songs/bonjovi/runaway.mp3
                    imagepath: imgs/bonjovi/runaway.jpg
                    creationDate: 08-02-1998
                    genre: ROCK
                    sizeInKB: 7449
                    deletedFlag: false
                  - songId: 97
                    title: Runaway
                    artist: Galantis
                    duration: 4226
                    filepath: songs/galantis/runaway.mp3
                    imagepath: imgs/galantis/runaway.jpg
                    creationDate: 08-02-2012
                    genre: HOUSE
                    sizeInKB: 5673
                    deletedFlag: false
  '/song/getByArtist/{artist}':
    get:
      summary: Returns all songs by artist
      description: >-
        This function returns all songs matching the artist. <br> Therefore an
        exact match is needed. For flexible searching see <br> /searchFor/
        below.
      tags:
        - song
      parameters:
        - in: path
          name: artist
          schema:
            type: string
          required: true
          example: Eminem
      responses:
        '200':
          description: Songs as JSON array
          content:
            application/json:
              schema:
                type: object
                example:
                  - songId: 33
                    title: Not Afraid
                    artist: Eminem
                    duration: 3488
                    filepath: songs/eminem/notafraid.mp3
                    imagepath: imgs/eminem/notafraid.jpg
                    creationDate: 08-02-2004
                    genre: HIPHOP
                    sizeInKB: 7449
                    deletedFlag: false
                  - songId: 1112
                    title: Lose Yourself
                    artist: Eminem
                    duration: 4226
                    filepath: songs/eminem/notafraid.mp3
                    imagepath: imgs/eminem/notafraid.jpg
                    creationDate: 08-02-2006
                    genre: HIPHOP
                    sizeInKB: 5673
                    deletedFlag: false
  '/song/getByGenre/{genre}':
    get:
      summary: Returns all songs by genre
      description: This function returns all songs matching the Genre.
      tags:
        - song
      parameters:
        - in: path
          name: genre
          schema:
            type: string
          required: true
          example: HIPHOP
      responses:
        '200':
          description: Songs as JSON array
          content:
            application/json:
              schema:
                type: object
                example:
                  - songId: 33
                    title: Not Afraid
                    artist: Eminem
                    duration: 3488
                    filepath: songs/eminem/notafraid.mp3
                    imagepath: imgs/eminem/notafraid.jpg
                    creationDate: 08-02-2004
                    genre: HIPHOP
                    sizeInKB: 7449
                    deletedFlag: false
                  - songId: 1112
                    title: Lose Yourself
                    artist: Eminem
                    duration: 4226
                    filepath: songs/eminem/notafraid.mp3
                    imagepath: imgs/eminem/notafraid.jpg
                    creationDate: 08-02-2006
                    genre: HIPHOP
                    sizeInKB: 5673
                    deletedFlag: false
  /song/getAllInDatabase:
    get:
      summary: Returns all songs in database
      description: This call returns ALL songs in database.
      tags:
        - song
      responses:
        '200':
          description: Songs as JSON array
          content:
            application/json:
              schema:
                type: object
                example:
                  - songId: 1
                    title: Runaway
                    artist: Bon Jovi
                    duration: 3488
                    filepath: songs/bonjovi/runaway.mp3
                    imagepath: imgs/bonjovi/runaway.jpg
                    creationDate: 08-02-1998
                    genre: ROCK
                    sizeInKB: 7449
                    deletedFlag: false
                  - songId: 2
                    title: Runaway
                    artist: Galantis
                    duration: 4226
                    filepath: songs/galantis/runaway.mp3
                    imagepath: imgs/galantis/runaway.jpg
                    creationDate: 08-02-2012
                    genre: HOUSE
                    sizeInKB: 5673
                    deletedFlag: true
  /song/uploadMeta:
    post:
      summary: Upload song metadata.
      description: >-
        This functions uploads song-metadata into a song repository.<br> It does
        NOT upload the mp3 file of a song. It only saves the <br> location of
        the song.
      tags:
        - song
      requestBody:
        required: true
        content:
          application/json:
            schema:
              type: object
              example:
                - title: Not Afraid
                  artist: Eminem
                  duration: 3488
                  filepath: songs/eminem/not_afraid.mp3
                  imagepath: imgs/eminem/not_afraid.jpg
                  creationDate: 08-02-2020
                  genre: HIPHOP
                  sizeInKB: 7449
      responses:
        '200':
          description: Song as JSON
          content:
            application/json:
              schema:
                type: object
                example:
                  - songId: 38
                    title: Not Afraid
                    artist: Eminem
                    duration: 3488
                    filepath: songs/eminem/not_afraid.mp3
                    imagepath: imgs/eminem/not_afraid.jpg
                    creationDate: 08-02-2020
                    genre: HIPHOP
                    sizeInKB: 7449
                    deletedFlag: false
  '/song/delete/{id}':
    delete:
      summary: Delete a song by its id.
      description: >-
        Using this function, the file path gets removed from the song and the
        deleted flag gets set. This function does NOT remove the song from the
        fileserver or from the songrepository.
      parameters:
        - in: path
          name: id
          schema:
            type: integer
          required: true
      tags:
        - song
      responses:
        '200':
          description: Song got deleted
  /playlist/create:
    post:
      summary: Creates a Playlist.
      tags:
        - playlist
      requestBody:
        required: true
        content:
          application/json:
            schema:
              type: object
              example:
                - name: '#batman'
                  ownerId: 114
                  imagepath: playlists/imgs/batman/batman01
      responses:
        '200':
          description: Song as JSON
          content:
            application/json:
              schema:
                type: object
                example:
                  - playlistId: 3552
                    name: '#batman'
                    ownerId: 114
                    duration: 0
                    imagepath: playlists/imgs/batman/batman01
                    creationDate: 08-12-2020
  '/playlist/addSong/{playlistId}/{songId}':
    put:
      summary: Adds an existing song to a playlist.
      tags:
        - playlist
      parameters:
        - in: path
          name: playlistId
          schema:
            type: integer
          required: true
        - in: path
          name: songId
          schema:
            type: integer
          required: true
      responses:
        '200':
          description: >-
            Returns the Playlist as JSON.<br> allSongsInPlaylist has the ids of
            all songs <br> which are added to the playlist including <br> the
            one just added.
          content:
            application/json:
              schema:
                type: object
                example:
                  - playlistId: 5
                    name: '#batman'
                    ownerId: 114
                    duration: 6338
                    imagepath: playlists/imgs/batman/batman01
                    creationDate: 08-12-2020
                    allSongsInPlaylist:
                      - 1
                      - 2
                      - 3
                      - 4
                      - 1
  '/playlist/getSongs/{playlistId}':
    get:
      summary: Returns all Songs in a playlist.
      tags:
        - playlist
      parameters:
        - in: path
          name: playlistId
          schema:
            type: integer
          required: true
          example: 4
      responses:
        '200':
          description: Returns the songs in a playlist as JSON.
          content:
            application/json:
              schema:
                type: object
                example:
                  - songId: 1
                    title: Runaway
                    artist: Bon Jovi
                    duration: 3488
                    filepath: songs/bonjovi/runaway.mp3
                    imagepath: imgs/bonjovi/runaway.jpg
                    creationDate: 08-02-1998
                    genre: ROCK
                    sizeInKB: 7449
                    deletedFlag: false
                  - songId: 2
                    title: Runaway
                    artist: Galantis
                    duration: 4226
                    filepath: songs/galantis/runaway.mp3
                    imagepath: imgs/galantis/runaway.jpg
                    creationDate: 08-02-2012
                    genre: HOUSE
                    sizeInKB: 5673
                    deletedFlag: true
  '/playlist/getByOwner/{ownerId}':
    get:
      summary: Returns all playlists a user has created.
      tags:
        - playlist
      parameters:
        - in: path
          name: ownerId
          schema:
            type: integer
          required: true
          example: 114
      responses:
        '200':
          description: >-
            Returns all Playlists as JSON. <br> The songs are linked with their
            ids.
          content:
            application/json:
              schema:
                type: object
                example:
                  - playlistId: 2411
                    name: '#batman'
                    ownerId: 114
                    duration: 6338
                    imagepath: playlists/imgs/batman/batman01
                    creationDate: 31-03-2020
                    allSongsInPlaylist:
                      - 34
                      - 333
                      - 453
                      - 2
                      - 2456
                  - playlistId: 1114
                    name: '#racemode'
                    ownerId: 114
                    duration: 1288
                    imagepath: playlists/imgs/batman/batman02
                    creationDate: 12-01-2020
                    allSongsInPlaylist:
                      - 788
                      - 765
                      - 344
                      - 12
                      - 23444
  '/playlist/getById/{playlistId}':
    get:
      summary: Returns one specific playlist.
      description: Returns one specific playlist (if found) as json.
      tags:
        - playlist
      parameters:
        - in: path
          name: playlistId
          schema:
            type: integer
          required: true
      responses:
        '200':
          description: Returns the playlist as JSON with all songs in it.
          content:
            application/json:
              schema:
                type: object
                example:
                  - playlistId: 2411
                    name: '#batman'
                    ownerId: 114
                    duration: 6338
                    imagepath: playlists/imgs/batman/batman01
                    creationDate: 31-03-2020
                    allSongsInPlaylist:
                      - 34
                      - 333
                      - 453
                      - 2
                      - 2456
  '/playlist/deleteSong/{playlistId}/{songId}':
    delete:
      summary: Deletes one specific song from a playlist.
      description: >-
        Deletes one specific song from a playlist.<br> If the song is added more
        than once to a playlist, <br> this functions deletes only the first
        found instance <br> of the song.
      tags:
        - playlist
      parameters:
        - in: path
          name: playlistId
          schema:
            type: integer
          required: true
        - in: path
          name: songId
          schema:
            type: integer
          required: true
      responses:
        '200':
          description: Returns the playlist as JSON with all remaining songs in it.
          content:
            application/json:
              schema:
                type: object
                example:
                  - playlistId: 2411
                    name: '#batman'
                    ownerId: 114
                    duration: 6338
                    imagepath: playlists/imgs/batman/batman01
                    creationDate: 31-03-2020
                    allSongsInPlaylist:
                      - 34
                      - 333
                      - 453
                      - 2
                      - 2456
  '/playlist/delete/{playlistId}':
    delete:
      summary: Deletes one specific playlist.
      description: >-
        Deletes one specific playlist from the repository.<br> This function
        cannot be undone. If a playlist gets deleted <br> using this function,
        it gets deleted permanently.
      tags:
        - playlist
      parameters:
        - in: path
          name: playlistId
          schema:
            type: integer
          required: true
      responses:
        '200':
          description: Returns the playlist as JSON with all remaining songs in it.
  /album/create:
    post:
      summary: Creates an album.
      description: >-
        by calling this POST-request an album can be created.<br> the album
        metadata should be specified in the requestbody.<br> it is not possible
        to create an album with the same title and artist twice.
      tags:
        - album
      requestBody:
        required: true
        content:
          application/json:
            schema:
              type: object
              example:
                - name: CB7
                  artist: Capital Bra
                  imagepath: imgs/capitalbra/cb7.jpg
                  releaseDate: 17-09-2020
                  allSongsInAlbum:
                    - 2
                    - 3
                    - 4
      responses:
        '200':
          description: Song as JSON
          content:
            application/json:
              schema:
                type: object
                example:
                  - albumId: 5
                    name: CB7
                    artist: Capital Bra
                    imagepath: imgs/capitalbra/cb7.jpg
                    releaseDate: 17-09-2020
                    allSongsInAlbum:
                      - 2
                      - 3
                      - 4
  '/album/addSong/{albumId}/{songId}':
    put:
      summary: Adds song to album.
      description: by calling this POST-request a song can be added to an album.<br>
      tags:
        - album
      parameters:
        - in: path
          name: albumId
          schema:
            type: integer
          example: 5
          description: 'specifies the album by its id, to which the song should be added'
          required: true
        - in: path
          name: songId
          schema:
            type: integer
          example: 6
          description: 'specifies the song by its id, which should be added to the album'
          required: true
      responses:
        '200':
          description: Added song as JSON
          content:
            application/json:
              schema:
                type: object
                example:
                  - songId: 6
                    title: Frühstück in Paris
                    artist: Capital Bra
                    duration: 163
                    filepath: songs/capitalbra/fruestueck_in_paris.mp3
                    imagepath: imgs/capitalbra/fruestueck_in_paris.jpg
                    creationDate: 17-09-2020
                    genre: RAP
                    sizeInKB: 7854
                    deletedFlag: false
  '/album/getById/{id}':
    get:
      summary: Returns album.
      description: This request returns the metadata of one specific album as JSON.
      tags:
        - album
      parameters:
        - in: path
          name: id
          schema:
            type: integer
          example: 5
          description: specifies the requested album by its id
          required: true
      responses:
        '200':
          description: Requested album as JSON
          content:
            application/json:
              schema:
                type: object
                example:
                  - albumId: '5,'
                    name: CB7
                    artist: Capital Bra
                    releaseDate: 17.09.2020
                    imagepath: img/path
                    allSongsInAlbum:
                      - 2
                      - 3
                      - 4
        '404':
          description: Album does not exist
          content:
            application/json:
              schema:
                type: object
                example:
                  - Album does not exist
  '/album/getSongs/{id}':
    get:
      summary: Returns songs in an album.
      description: This request returns all songs in one specific album as JSON<br>
      tags:
        - album
      parameters:
        - in: path
          name: id
          schema:
            type: integer
          example: 5
          description: specifies the requested album by its id
          required: true
      responses:
        '200':
          description: Requested album as JSON
          content:
            application/json:
              schema:
                type: object
                example:
                  - songId: 6
                    title: Frühstück in Paris
                    artist: Capital Bra
                    duration: 163
                    filepath: songs/capitalbra/fruestueck_in_paris.mp3
                    imagepath: imgs/capitalbra/fruestueck_in_paris.jpg
                    creationDate: 17-09-2020
                    genre: RAP
                    sizeInKB: 7854
                    deletedFlag: false
                  - songId: 7
                    title: Einsam an der Spitze
                    artist: Capital Bra
                    duration: 184
                    filepath: songs/capitalbra/einsam_an_der_spitze.mp3
                    imagepath: imgs/capitalbra/einsam_an_der_spitze.jpg
                    creationDate: 17-09-2020
                    genre: RAP
                    sizeInKB: 5378
                    deletedFlag: false
                  - ...
        '404':
          description: Album does not exist
          content:
            application/json:
              schema:
                type: object
                example:
                  - Album does not exist
  '/album/delete/{id}':
    delete:
      summary: Delete an album.
      description: by calling this DELETE-request an album can be deleted<br>
      tags:
        - album
      parameters:
        - in: path
          name: id
          schema:
            type: integer
          example: 5
          description: specifies the requested album by its id
          required: true
      responses:
        '404':
          description: Album does not exist
          content:
            application/json:
              schema:
                type: object
                example:
                  - Album does not exist
  '/album/deleteSong/{albumId}/{songId}':
    delete:
      summary: Delets a song from an album.
      description: by calling this DELETE-request a song can be deleted from an album.
      tags:
        - album
      parameters:
        - in: path
          name: albumId
          schema:
            type: integer
          example: 5
          description: 'specifies the album by its id, from which the song should be deleted'
          required: true
        - in: path
          name: songId
          schema:
            type: integer
          example: 8
          description: 'specifies the song by its id, which should be deleted from the album'
          required: true
      responses:
        '200':
          description: Returns the album with all remaining songs
          content:
            application/json:
              schema:
                type: object
                example:
                  - albumId: '5,'
                    name: CB7
                    artist: Capital Bra
                    releaseDate: 17.09.2020
                    imagepath: img/path
                    allSongsInAlbum:
                      - 6
                      - 7
                      - 9
                      - 10
                      - ...
        '404':
          description: Album does not exist
          content:
            application/json:
              schema:
                type: object
                example:
                  - Album does not exist
  '/searchFor/{searchterm}':
    get:
      summary: Returns the top 5 songs matching the search.
      description: >-
        This function returns the top 5 songs containing<br> the search term
        either in 'title' or 'artist'. <br> This function is build using a SQL
        statement for <br> fast searching in song database. <br> The searchterm
        is case-insensitive.
      tags:
        - search
      parameters:
        - in: path
          name: searchterm
          schema:
            type: string
          required: true
          example: Eminem
      responses:
        '200':
          description: Songs as JSON array
          content:
            application/json:
              schema:
                type: object
                example:
                  - songId: 33
                    title: Not Afraid
                    artist: Eminem
                    duration: 3488
                    filepath: songs/eminem/notafraid.mp3
                    imagepath: imgs/eminem/notafraid.jpg
                    creationDate: 08-02-2004
                    genre: HIPHOP
                    sizeInKB: 7449
                    deletedFlag: false
                  - songId: 1112
                    title: Lose Yourself
                    artist: Eminem
                    duration: 4226
                    filepath: songs/eminem/notafraid.mp3
                    imagepath: imgs/eminem/notafraid.jpg
                    creationDate: 08-02-2006
                    genre: HIPHOP
                    sizeInKB: 5673
                    deletedFlag: false
FROM maven:3.6.3-jdk-11-slim AS MAVEN_BUILD
ARG SPRING_ACTIVE_PROFILE
COPY pom.xml /build/
COPY src /build/src/
WORKDIR /build/
RUN mvn clean install -Dspring.profiles.active=$SPRING_ACTIVE_PROFILE && mvn package -B -e -Dspring.profiles.active=$SPRING_ACTIVE_PROFILE


FROM openjdk:11-slim
EXPOSE 9090
WORKDIR /app
COPY --from=MAVEN_BUILD /build/target/songservice-1.4.5.jar /app/songservice-1.4.5.jar

ENV EUREKA_HOST=${EUREKA_HOST:-"localhost:8761"}

ENTRYPOINT ["java","-jar","/app/songservice-1.4.5.jar"]
